#ifndef CUSTLAADANAZA_H
#define CUSTLAADANAZA_H
/* ========================================================================
FILE: CUSTLAADANAZA

Copyright (c) 2017 by Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.        
=========================================================================== */

#ifndef TARGLAADANAZA_H
   #include "targlaadanaza.h"
#endif

#define FEATURE_QFUSE_PROGRAMMING
#define FEATURE_DLOAD_MEM_DEBUG
#define IMAGE_KEY_SBL1_IMG_DEST_ADDR SCL_SBL1_CODE_BASE
#define BOOT_TEMP_CHECK_THRESHOLD_DEGC
#define FEATURE_TPM_HASH_POPULATE
#define FEATURE_BOOT_LOGDUMP_PARTITION_TO_SD_CARD 




#endif /* CUSTLAADANAZA_H */
