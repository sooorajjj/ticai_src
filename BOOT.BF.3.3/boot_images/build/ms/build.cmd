@echo off
rem ==========================================================================
rem
rem  CBSP Buils system
rem
rem  General Description
rem     build batch file.
rem
rem Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem
rem --------------------------------------------------------------------------
rem
rem                      EDIT HISTORY FOR FILE
rem
rem  This section contains comments describing changes made to the module.
rem  Notice that changes are listed in reverse chronological order.
rem
rem when       who     what, where, why
rem --------   ---     ---------------------------------------------------------
rem 04/22/16   qbz    Add DDI compiling support for all platforms
rem 01/07/16   sk     Disabled RUMI flavour in Jacala.
rem 01/01/16   sk     Disabled RUMI flavour in Feero-Lite.
rem 12/10/15   sk     Enabled fire-hose compilation to Jacala.
rem 11/27/15   yps    Add 8952 DDI compiling commands
rem 11/17/15   yps    Add 8953 DDI compiling commands
rem 10/29/15   lm     Initial version
rem ==========================================================================

setlocal

rem GOTO USAGE

:BEGIN 
rem ========  If we have a set environment batch file, call it to set KLOCWORK and RVCT version.========
IF EXIST setenv.cmd CALL setenv.cmd

rem ===== Setup Paths=====================================================
SET BUILD_ROOT=%~dp0..\..
SET CORE_BSP_ROOT=%BUILD_ROOT%\core\bsp
SET TOOLS_SCONS_ROOT=%BUILD_ROOT%\tools\build\scons
SET TARGET_FAMILY=
SET BUILD_ID=
SET isProd=0
SET  cmd=

setlocal 

IF "%1"=="" (
  @ECHO %1 !!!!!!---- ERR: TARGET_FAMILY is missing. See build.cmd -usage for usage-----!!!!!!
  GOTO USAGE
)
IF "%1"=="-usage" GOTO USAGE

:LOOP
IF "%1"=="" GOTO CONTINUE 

IF "%1"=="TARGET_FAMILY" (
  SET TARGET_FAMILY=%2
) 
IF "%1"=="BUILD_ID" (
  SET BUILD_ID=%2
)
IF "%1"=="--prod" (
  SET isProd=1
)
SHIFT
GOTO LOOP 

:CONTINUE 
REM Create build log
rem @echo CALL %TOOLS_SCONS_ROOT%\build\rename-log.cmd  buildlog
rem CALL %TOOLS_SCONS_ROOT%\build\rename-log.cmd buildlog
rem echo Start Date=%startdate%, Time=%starttime% > buildlog-log.txt

rem Adding  warning = error flags 
SET "cmd=%* CFLAGS=--diag_error=warning"
IF %isProd%== 1 SET "cmd=%cmd:--prod =% USES_FLAGS=USES_NO_STRIP_NO_ODM,USES_NO_DEBUG"


rem set build command base on target name
SET BUILD_CMD="b%TARGET_FAMILY%.cmd"
  
IF /I "%TARGET_FAMILY%" == "8952" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 ( rem compiling prod build for 8952
      rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=SAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=SAASANAZ  %cmd% 
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-emmc.txt
	  CALL %BUILD_CMD% ddr_debug BUILD_ID=SAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt
	)ELSE ( rem  normal flow/default 8952 build
	  rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=SAAAANAA %cmd%
	  rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=SAASANAA %cmd%
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-emmc.txt
	  CALL %BUILD_CMD% ddr_debug BUILD_ID=SAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt)
	)ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:SAASANAZ
   rem compiling saasanaa 8952-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:SAAAANAZ
   rem compiling saasanaa 8952-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:SAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
:SAASANAA
   rem compiling saasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:SAAAANAA
   rem compiling saasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:SAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
  )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  GOTO END
)


IF /I "%TARGET_FAMILY%" == "8953" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 ( rem compiling prod build for 8953
      rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=JAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi.txt
      CALL %BUILD_CMD% boot jsdcc BUILD_ID=JAASANAZ  %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-emmc.txt
      rem CALL %BUILD_CMD% ddr_debug BUILD_ID=JAAAANAZ  %cmd%
      rem TYPE build-log.txt >> buildlog-ddi-rumi.txt
      CALL %BUILD_CMD% ddr_debug BUILD_ID=JAASANAZ  %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt  
	)ELSE ( rem  normal flow/default 8953 build
	  rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=JAAAANAA %cmd%
	  rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=JAASANAA %cmd%
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-emmc.txt
	  rem CALL %BUILD_CMD% ddr_debug BUILD_ID=JAAAANAA %cmd%
	  rem TYPE build-log.txt >> buildlog-ddi-rumi.txt
	  CALL %BUILD_CMD% ddr_debug BUILD_ID=JAASANAA %cmd%
          TYPE build-log.txt >> buildlog-ddi.txt)
	)ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:JAASANAZ
   rem compiling JAASANAA 8953-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:JAAAANAZ
   rem compiling JAASANAA 8953-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:JAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
:JAASANAA
   rem compiling JAASANAA emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:JAAAANAA
   rem compiling JAASANAA emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:JAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%  
   goto END
  )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  GOTO END
)

IF /I "%TARGET_FAMILY%" == "8976" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 (  
    rem compiling prod build for 8976
      CALL %BUILD_CMD% boot jsdcc BUILD_ID=EAAAANAZ  %cmd% 
      TYPE build-log.txt >> buildlog-rumi.txt  
 	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=EAASANAZ  ?%cmd%
      TYPE build-log.txt >> buildlog-emmc.txt
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-deviceprogrammer.txt
	  CALL %BUILD_CMD% ddr_debug BUILD_ID=EAADANAZ  %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt  
    )ELSE ( rem  normal flow/default 8976 build
      CALL %BUILD_CMD% boot jsdcc BUILD_ID=EAAAANAA %cmd%
 	  TYPE build-log.txt >> buildlog-rumi.txt
 	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=EAASANAA %cmd%
      TYPE build-log.txt >> buildlog-emmc.txt
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-deviceprogrammer.txt
	  CALL %BUILD_CMD% ddr_debug BUILD_ID=EAADANAA  %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt
    )
  )ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:EAAAANAZ
   rem compiling EAAAANAZ 8976-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:EAASANAZ
   rem compiling EAASANAZ 8976-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:EAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID%  %cmd%
   TYPE build-log.txt >> buildlog-ddi.txt
   goto END
:EAAAANAA
   rem compiling EAAAANAA emmc internal
   CALL %BUILD_CMD% boot BUILD_ID=EAAAANAA %cmd%
   goto END
:EAASANAA
   rem compiling EAASANAA emmc internal
   CALL %BUILD_CMD% boot BUILD_ID=EAASANAA %cmd%
   goto END
:EAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID%  %cmd%
   TYPE build-log.txt >> buildlog-ddi.txt
   goto END
  )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  GOTO END
)

IF /I "%TARGET_FAMILY%" == "8952_8976" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 ( rem compiling prod build for 8952
      rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=SAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL b8952.cmd boot jsdcc BUILD_ID=SAASANAZ  %cmd% 
	  TYPE build-log.txt >> buildlog-emmc-8952.txt
	  CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8952.txt
	  CALL b8952.cmd ddr_debug BUILD_ID=SAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi-8952.txt
	  rem compiling prod build for 8976
      rem CALL b8976.cmd boot jsdcc BUILD_ID=EAAAANAZ  %cmd% 
      TYPE build-log.txt >> buildlog-rumi-8976.txt  
 	  CALL b8976.cmd boot jsdcc BUILD_ID=EAASANAZ  %cmd%
      TYPE build-log.txt >> buildlog-emmc-8976.txt
	  CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-deviceprogrammer-8976.txt
      CALL b8976.cmd ddr_debug BUILD_ID=EAADANAZ  %cmd%
      TYPE build-log.txt >> buildlog-ddi-8976.txt
	)ELSE ( rem  normal flow/default 8952 build
	  rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=SAAAANAA %cmd%
	  rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL b8952.cmd boot jsdcc BUILD_ID=SAASANAA %cmd%
	  TYPE build-log.txt >> buildlog-emmc-8952.txt
	  CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL b8952.cmd deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8952.txt
	  CALL b8952.cmd ddr_debug BUILD_ID=SAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi-8952.txt
	  rem compiling build for 8976
	  rem CALL b8976.cmd boot jsdcc BUILD_ID=EAAAANAA %cmd%
 	  rem TYPE build-log.txt >> buildlog-rumi-8976.txt
 	  CALL b8976.cmd boot jsdcc BUILD_ID=EAASANAA %cmd%
      TYPE build-log.txt >> buildlog-emmc-8976.txt
	  CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL b8976.cmd deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8976.txt
	  CALL b8976.cmd ddr_debug BUILD_ID=EAADANAA  %cmd%
      TYPE build-log.txt >> buildlog-ddi-8976.txt
	  )
	)
ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:EAASANAZ
   rem compiling EAASANAZ 8976-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-emmc-8976.txt
   goto END
:EAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   TYPE build-log.txt >> buildlog-deviceprogrammer-8976.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8976.txt
   goto END
:EAASANAA
   rem compiling EAASANAA emmc internal
   CALL %BUILD_CMD% boot BUILD_ID=EAASANAA %cmd%
   TYPE build-log.txt >> buildlog-emmc-8976.txt
   goto END
:EAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
   TYPE build-log.txt >> buildlog-deviceprogrammer-8976.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8976.txt
   goto END
:SAASANAZ
   rem compiling saasanaa 8952-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-emmc-8952.txt
   goto END
:SAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   TYPE build-log.txt >> buildlog-deviceprogrammer-8952.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8952.txt
   goto END
:SAASANAA
   rem compiling saasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-emmc-8952.txt
   goto END
:SAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
   TYPE build-log.txt >> buildlog-deviceprogrammer-8952.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8952.txt
   GOTO END
 )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  
)
IF /I "%TARGET_FAMILY%" == "8937_8917" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 ( rem compiling prod build for 8937
	  rem CALL b8937.cmd boot jsdcc BUILD_ID=FAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi.txt  
	  CALL b8937.cmd boot jsdcc BUILD_ID=FAASANAZ  %cmd% 
	  TYPE build-log.txt >> buildlog-emmc-8937.txt
	  CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
	  CALL b8937.cmd ddr_debug BUILD_ID=FAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi-8937.txt
	  rem compiling prod build for 8917
      rem CALL b8917.cmd boot jsdcc BUILD_ID=LAAAANAZ  %cmd% 
	  rem CALL b8917.cmd boot jsdcc BUILD_ID=LAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi-8917.txt  
	  CALL b8917.cmd boot jsdcc BUILD_ID=LAASANAZ  %cmd% 
	  TYPE build-log.txt >> buildlog-emmc-8917.txt
	  CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
	  CALL b8917.cmd ddr_debug BUILD_ID=LAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi-8917.txt
	)ELSE ( rem  normal flow/default 8937 build
	  rem CALL b8937.cmd boot jsdcc BUILD_ID=FAAAANAA %cmd%
	  rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL b8937.cmd boot jsdcc BUILD_ID=FAASANAA %cmd%
	  TYPE build-log.txt >> buildlog-emmc-8937.txt
	  CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
	  CALL b8937.cmd ddr_debug BUILD_ID=FAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi-8937.txt
	  rem compiling build for 8917
	  rem CALL b8937.cmd boot jsdcc BUILD_ID=LAAAANAA %cmd%%
 	  rem TYPE build-log.txt >> buildlog-rumi-8917.txt
	  rem CALL b8917.cmd boot jsdcc BUILD_ID=LAAAANAA %cmd%
 	  rem TYPE build-log.txt >> buildlog-rumi-8917.txt
	  CALL b8917.cmd boot jsdcc BUILD_ID=LAASANAA %cmd%	
	  TYPE build-log.txt >> buildlog-emmc-8917.txt
	  CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
	  CALL b8917.cmd ddr_debug BUILD_ID=LAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi-8917.txt
	  )
	)
ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:FAASANAZ
   rem compiling faasanaa 8937-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-emmc-8937.txt
   goto END
:FAAAANAZ
   rem compiling faaaanaa 8937-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-rumi-8937.txt
   goto END
:FAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8937.txt
   goto END
:FAASANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-emmc-8937.txt
   goto END   
:FAAAANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-rumi-8937.txt
   goto END
:FAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%
   TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8937.txt
   goto END
:LAASANAZ
   rem compiling faasanaa 8917-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-emmc-8917.txt
   goto END
:LAAAANAZ
   rem compiling faasanaa 8917-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-rumi-8917.txt
   goto END
:LAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8917.txt
   goto END
:LAASANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-emmc-8917.txt
   goto END
:LAAAANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-rumi-8917.txt
   goto END
:LAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%  
   TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8917.txt
   GOTO END
 )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  
)

IF /I "%TARGET_FAMILY%" == "8937_8917_8953" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 ( rem compiling prod build for 8937
	  rem CALL b8937.cmd boot jsdcc BUILD_ID=FAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi.txt  
	  CALL b8937.cmd boot jsdcc BUILD_ID=FAASANAZ  %cmd% 
	  TYPE build-log.txt >> buildlog-emmc-8937.txt
	  CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
	  CALL b8937.cmd ddr_debug BUILD_ID=FAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi-8937.txt
	  rem compiling prod build for 8917
      rem CALL b8917.cmd boot jsdcc BUILD_ID=LAAAANAZ  %cmd% 
	  rem CALL b8917.cmd boot jsdcc BUILD_ID=LAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi-8917.txt  
	  CALL b8917.cmd boot jsdcc BUILD_ID=LAASANAZ  %cmd% 
	  TYPE build-log.txt >> buildlog-emmc-8917.txt
	  CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
	  CALL b8917.cmd ddr_debug BUILD_ID=LAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi-8917.txt
	  CALL b8953.cmd boot jsdcc BUILD_ID=JAASANAZ  %cmd% 
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-emmc.txt
      CALL b8953.cmd ddr_debug BUILD_ID=JAASANAZ  %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt  
	  
	)ELSE ( rem  normal flow/default 8937 build
	  rem CALL b8937.cmd boot jsdcc BUILD_ID=FAAAANAA %cmd%
	  rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL b8937.cmd boot jsdcc BUILD_ID=FAASANAA %cmd%
	  TYPE build-log.txt >> buildlog-emmc-8937.txt
	  CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL b8937.cmd deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
	  CALL b8937.cmd ddr_debug BUILD_ID=FAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi-8937.txt	  
	  rem compiling build for 8917
	  rem CALL b8937.cmd boot jsdcc BUILD_ID=LAAAANAA %cmd%%
 	  rem TYPE build-log.txt >> buildlog-rumi-8917.txt
	  rem CALL b8917.cmd boot jsdcc BUILD_ID=LAAAANAA %cmd%
 	  rem TYPE build-log.txt >> buildlog-rumi-8917.txt
	  CALL b8917.cmd boot jsdcc BUILD_ID=LAASANAA %cmd%	
	  TYPE build-log.txt >> buildlog-emmc-8917.txt
	  CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL b8917.cmd deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
	  CALL b8917.cmd ddr_debug BUILD_ID=LAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi-8917.txt
	  CALL b8953.cmd boot jsdcc BUILD_ID=JAASANAA  %cmd% 
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAA %cmd%,USES_DEVPRO_DDR 
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAA %cmd%,USES_DEVPRO_DDR_SEC  
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAA %cmd%,USES_DEVPRO_LITE 
      CALL b8953.cmd deviceprogrammer_ddr BUILD_ID=JAADANAA %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-emmc.txt
      CALL b8953.cmd ddr_debug BUILD_ID=JAASANAA  %cmd%
      TYPE build-log.txt >> buildlog-ddi-8953.txt  
	  
	  )
	)
ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:FAASANAZ
   rem compiling faasanaa 8937-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-emmc-8937.txt
   goto END
:FAAAANAZ
   rem compiling faaaanaa 8937-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-rumi-8937.txt
   goto END
:FAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8937.txt
   goto END
:FAASANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-emmc-8937.txt
   goto END   
:FAAAANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-rumi-8937.txt
   goto END
:FAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%
   TYPE build-log.txt >> buildlog-deviceprogrammer-8937.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8937.txt
   goto END
:LAASANAZ
   rem compiling faasanaa 8917-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-emmc-8917.txt
   goto END
:LAAAANAZ
   rem compiling faasanaa 8917-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   TYPE build-log.txt >> buildlog-rumi-8917.txt
   goto END
:LAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8917.txt
   goto END
:LAASANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-emmc-8917.txt
   goto END
:LAAAANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-rumi-8917.txt
   goto END
:LAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%  
   TYPE build-log.txt >> buildlog-deviceprogrammer-8917.txt
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   TYPE build-log.txt >> buildlog-ddi-8917.txt
   GOTO END

:JAASANAZ
   rem compiling JAASANAA 8953-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:JAAAANAZ
   rem compiling JAASANAA 8953-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:JAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
:JAASANAA
   rem compiling JAASANAA emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:JAAAANAA
   rem compiling JAASANAA emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:JAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%  
   goto END
 )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  
)


IF /I "%TARGET_FAMILY%" == "8937" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 ( rem compiling prod build for 8937
	  rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=FAAAANAZ  %cmd% 
      rem TYPE build-log.txt >> buildlog-rumi.txt  
	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=FAASANAZ  %cmd% 
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-emmc.txt
      CALL %BUILD_CMD% ddr_debug BUILD_ID=FAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt
	)ELSE ( rem  normal flow/default 8937 build
	  rem CALL %BUILD_CMD% boot jsdcc BUILD_ID=FAAAANAA %cmd%
 	  rem TYPE build-log.txt >> buildlog-rumi.txt
	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=FAASANAA %cmd%
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-emmc.txt
	  CALL %BUILD_CMD% ddr_debug BUILD_ID=FAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt
	  )
	)ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:FAASANAZ
   rem compiling faasanaa 8937-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:FAAAANAZ
   rem compiling faasanaa 8937-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:FAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
:FAASANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:FAAAANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:FAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%  
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
  )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  GOTO END
)

IF /I "%TARGET_FAMILY%" == "8917" ( 
IF "%BUILD_ID%" == "" (
    IF %isProd% == 1 ( rem compiling prod build for 8917
	  REM CALL %BUILD_CMD% boot jsdcc BUILD_ID=LAAAANAZ  %cmd% 
      REM TYPE build-log.txt >> buildlog-rumi.txt  
	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=LAASANAZ  %cmd% 
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_DDR 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_DDR_SEC  
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_LITE 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAZ %cmd%,USES_DEVPRO_LITE_SEC
      TYPE build-log.txt >> buildlog-emmc.txt
      CALL %BUILD_CMD% ddr_debug BUILD_ID=LAADANAZ %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt
	)ELSE ( rem  normal flow/default 8917 build
	  REM CALL %BUILD_CMD% boot jsdcc BUILD_ID=LAAAANAA %cmd%
 	  REM TYPE build-log.txt >> buildlog-rumi.txt
	  CALL %BUILD_CMD% boot jsdcc BUILD_ID=LAASANAA %cmd%	  
	  CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE %cmd% 
      CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd% 
	  TYPE build-log.txt >> buildlog-emmc.txt
	  CALL %BUILD_CMD% ddr_debug BUILD_ID=LAADANAA %cmd%
      TYPE build-log.txt >> buildlog-ddi.txt)
	)ELSE (  rem compile base on Build ID
    goto  %BUILD_ID%
:LAASANAZ
   rem compiling faasanaa 8917-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:LAAAANAZ
   rem compiling faasanaa 8917-prod emmc
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd% 
   goto END
:LAADANAZ
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_DDR_SEC  
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% %cmd%,USES_DEVPRO_LITE_SEC
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
:LAASANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:LAAAANAA
   rem compiling faasanaa emmc internal
   CALL %BUILD_CMD% boot jsdcc BUILD_ID=%BUILD_ID% %cmd%
   goto END
:LAADANAA
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_DDR_SEC %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE %cmd% 
   CALL %BUILD_CMD% deviceprogrammer_ddr BUILD_ID=%BUILD_ID% USES_FLAGS=USES_DEVPRO_LITE_SEC %cmd%
   CALL %BUILD_CMD% ddr_debug BUILD_ID=%BUILD_ID% %cmd%
   goto END
  )  
  TYPE %BUILD_CMD%-log.txt >> buildlog-log.txt
  GOTO END
)

:USAGE   
rem =========  Usage ====================================================
@ECHO.
@ECHO =============== USAGE : build.cmd  ========================================
@ECHO # This script compiles boot binaries for ONE of the Bear family e.g. 8952.  
@ECHO # Target must be specific by TARGET_FAMILY
@ECHO .
@ECHO build.cmd -h                         # Showing this help menu
@ECHO build.cmd                            # Error - Missing Target family
@ECHO build.cmd  TARGET_FAMILY=8952        # compiling 8952
@ECHO build.cmd  -c TARGET_FAMILY=8952     # clean 8952 binaries
@ECHO build.cmd  TARGET_FAMILY=8952 --prod # compiling 8952 production build
@ECHO . 
@ECHO # To build individual module , use the target specific build comand
@ECHO .
@ECHO b8952.cmd sbl1                       # build sbl1 for 8952
@ECHO . 
@ECHO ===============================================================

rem === Delay 3 seconds  ==== 
rem choice /c delay /d y /t 3  >null

GOTO END

:END 
rem @echo Start Date=%startdate%, Time=%starttime% - End Date=%enddate%, Time=%endtime% >> buildlog-log.txt
endlocal






