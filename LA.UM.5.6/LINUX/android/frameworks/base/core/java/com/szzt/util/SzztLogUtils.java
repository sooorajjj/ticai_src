package com.szzt.util;

import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by yxljl1314 on 2017/12/13.
 */

public class SzztLogUtils {
    private static final String TAG = "SzztLog";
    public static boolean isDebugMode = false;
    public static boolean isDebug(){
        return isDebugMode;
    }
    public static void e(String tag,String msg){
        if(TextUtils.isEmpty(msg)){
            return;
        }
        Log.e(TAG, tag+","+"["+Thread.currentThread().getStackTrace()[3].getMethodName()+","+
                Thread.currentThread().getStackTrace()[3].getFileName()+","+Thread.currentThread().getStackTrace()[3].getLineNumber()+"] "
        + msg);
    }
    public static void i(String tag,String msg){
        if(TextUtils.isEmpty(msg)){
            return;
        }
        Log.i(TAG, tag+","+"["+Thread.currentThread().getStackTrace()[3].getMethodName()+","+
                Thread.currentThread().getStackTrace()[3].getFileName()+","+Thread.currentThread().getStackTrace()[3].getLineNumber()+"] "
                + msg);
    }
    public static void w(String tag,String msg){
        if(TextUtils.isEmpty(msg)){
            return;
        }
        Log.w(TAG, tag+","+"["+Thread.currentThread().getStackTrace()[3].getMethodName()+","+
                Thread.currentThread().getStackTrace()[3].getFileName()+","+Thread.currentThread().getStackTrace()[3].getLineNumber()+"] "
                + msg);
    }
    public static void d(String tag,String msg){
        if(TextUtils.isEmpty(msg)){
            return;
        }
        Log.d(TAG, tag+","+"["+Thread.currentThread().getStackTrace()[3].getMethodName()+","+
                Thread.currentThread().getStackTrace()[3].getFileName()+","+Thread.currentThread().getStackTrace()[3].getLineNumber()+"] "
                + msg);
    }
    public static void v(String tag,String msg){
        if(TextUtils.isEmpty(msg)){
            return;
        }
        Log.v(TAG, tag+","+"["+Thread.currentThread().getStackTrace()[3].getMethodName()+","+
                Thread.currentThread().getStackTrace()[3].getFileName()+","+Thread.currentThread().getStackTrace()[3].getLineNumber()+"] "
                + msg);
    }

    public static String getCurrentLogcatInfo(){
        StringBuilder sb = new StringBuilder(4096);
        InputStreamReader input = null;
        try {
            java.lang.Process logcat = new ProcessBuilder(
                    "/system/bin/timeout", "-k", "15s", "10s",
                    "/system/bin/logcat", "-v", "threadtime", "-b", "events", "-b", "system",
                    "-b", "main", "-b", "crash", "-t", String.valueOf(500))
                    .redirectErrorStream(true).start();

            try { logcat.getOutputStream().close(); } catch (IOException e) {}
            try { logcat.getErrorStream().close(); } catch (IOException e) {}
            input = new InputStreamReader(logcat.getInputStream());

            int num;
            char[] buf = new char[8192];
            while ((num = input.read(buf)) > 0) sb.append(buf, 0, num);
        } catch (IOException e) {
            SzztLogUtils.e(TAG, "Error running logcat ,"+e.toString());
        } finally {
            if (input != null) try { input.close(); } catch (IOException e) {}
        }
        return sb.toString();
    }

    public static String getCurrentMemInfo(){
        StringBuilder sb = new StringBuilder(4096);
        InputStreamReader input = null;
        try {
            java.lang.Process meminfo = new ProcessBuilder(
                    "/system/bin/dumpsys","meminfo")
                    .redirectErrorStream(true).start();

            try { meminfo.getOutputStream().close(); } catch (IOException e) {}
            try { meminfo.getErrorStream().close(); } catch (IOException e) {}
            input = new InputStreamReader(meminfo.getInputStream());

            int num;
            char[] buf = new char[8192];
            while ((num = input.read(buf)) > 0) sb.append(buf, 0, num);
        } catch (IOException e) {
            SzztLogUtils.e(TAG, "Error running dumpsys meminfo ,"+e.toString());
        } finally {
            if (input != null) try { input.close(); } catch (IOException e) {}
        }
        return sb.toString();
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }
}
