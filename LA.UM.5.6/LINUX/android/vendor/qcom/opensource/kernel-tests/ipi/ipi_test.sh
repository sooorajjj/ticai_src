# Copyright (c) 2014, 2016, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



testpath=/data/kernel-tests

if [ -d /system/lib/modules/ ]; then
	modpath=/system/lib/modules
else
	modpath=/kernel-tests/modules/lib/modules/$(uname -r)/extra
fi

# Mount debugfs
mount -t debugfs nodev /sys/kernel/debug 2>/dev/null

core_ctl_mod_name=core_ctl
core_ctl_mod=/system/lib/modules/core_ctl.ko
core_ctl=0

ipi_test_mod=${modpath}/ipi_test_module.ko
ipi_test_module_name=ipi_test_module
ipi_test_iter="/sys/kernel/debug/ipi_test/iteration"
ipi_test_result="/sys/kernel/debug/ipi_test/result"
ipi_test_start="/sys/kernel/debug/ipi_test/start"

# Begin script execution here
# Sets 10000 as a default interation number of times to test IPI
test_iter=10000
test_type=1
verbose=0

while [ $# -gt 0 ]
do
	case $1 in
	-n | --nominal)
		shift 1
		;;
	-a | --adversarial)
		shift 1
		;;
	-r | --repeatability)
		test_iter=1000000
		shift 1
		;;
	-s | --stress)
		test_type=3
		test_iter=500000
		shift 1
		;;
	-v | --verbose)
		verbose=1
		shift 1
		;;
	-t | --times)
		test_iter=$2
		shift 2
		;;
	-e | --test_type)
		test_type=$2
		shift 2
		;;
	-h | --help | *)
	echo " Usage: $0 [[(-t | --times) <iterations>] [(-e | --test_type) <test_type>]"
	echo "		 [-s | --stress] [-v | --verbose]"
	echo " iterations: the times number of ipi to test"
	echo " test_type: "
	echo "1. Use smp_single: send IPI one by one to all cores"
	echo "2. Use smp_many: send IPI to all cores at once"
	echo "3. Use Both: (1 & 2)"
	echo "Recommended maximum iterations: 1000000"
	exit 1
	;;
	esac
done

ipi_test_init(){
	# insert ipi_test_module
	insmod $ipi_test_mod
	if [ $? -ne 0 ]; then
		echo "ERROR: failed to load module $ipi_test_mod"
		# re insert core_ctl
		if [ "$core_ctl" -eq 1 ]; then
			echo "Re insert core_ctl"
			insmod $core_ctl_mod
			if [ $? -ne 0 ]; then
				echo "ERROR: failed to load module $core_ctl_mod"
			fi
		fi
		exit 1
	fi
}

ipi_test_exit(){
	#remove ipi_test_module after test
	rmmod $ipi_test_module_name > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		echo "Failed to remove module $ipi_test_mod"
	fi

	if [ "$core_ctl" -eq 1 ]; then
		echo "Re insert core_ctl"
		insmod $core_ctl_mod
		if [ $? -ne 0 ]; then
			echo "Failed to load module $core_ctl_mod"
		fi
	fi
}

ipi_test(){

	# Remove core ctl to avoid cpu hotplug in middle of the test.
	echo "Removing core_ctl.."
	lsmod | grep "$core_ctl_mod_name" >> /dev/null
	if [ $? -eq 0 ]; then
		core_ctl=1
                rmmod $core_ctl_mod_name
		if [ $? -ne 0 ]; then
                        echo "ERROR: failed to remove module $core_ctl_mod_name"
			exit 1
                fi
        fi

	if [ "$test_iter" -gt 1000000 ]; then
		echo "Recommended iterations are: 1000000"
		echo "so truncated to max iterations."
		test_iter=1000000
	fi

	echo "IPI test starting"
	ipi_test_init

	echo $test_iter > ${ipi_test_iter}
	echo $test_type > ${ipi_test_start}
	if [ $? -eq 0 ]; then
		echo "Done.. "
		echo "Max time taken for IPI: (nsec)"
		cat ${ipi_test_start}
	else
		ipi_test_exit
		echo "Test Result: FAILED"
		exit 1
	fi

	if [ "$verbose" -eq 1 ]; then
		cat ${ipi_test_result}
	fi

	ipi_test_exit
	echo "Test Result: PASS"
	exit 0
}

ipi_test $test_iter $test_type
