LOCAL_MODULE        := szzt_contexts
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/etc

SZZT_APP_PACKAGENAME += com.cpos.sdk

LOCAL_POST_PROCESS_COMMAND :=$(shell mkdir -p $(LOCAL_MODULE_PATH))
LOCAL_POST_PROCESS_COMMAND :=$(shell rm $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE))
LOCAL_POST_PROCESS_COMMAND :=$(shell touch $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE))
LOCAL_POST_PROCESS_COMMAND :=$(foreach c_file,$(SZZT_APP_PACKAGENAME), $(shell echo "user=_app seinfo=platform name=$(c_file) domain=szzt_app type=szzt_data_file" >> $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)))
LOCAL_POST_PROCESS_COMMAND :=$(foreach c_file,$(SZZT_APP_PACKAGENAME), $(shell echo "user=system seinfo=platform name=$(c_file) domain=szzt_app type=szzt_data_file" >> $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)))

include $(call all-subdir-makefiles)
