#########################################################################
# File Name: auto_out.sh
# Author: 
# mail: 
# Created Time: 2016年11月22日 星期二 16时41分09秒
#########################################################################
#!/bin/bash

PROJECT_DT1=$(date +%Y%m%d)
PROJECT_DT2=$(date +%Y%m%d%H%m)

prodir=$(pwd)

cd MSM8953.LA.2.0/common/build/
isSecure="FH"
if [ $1 = "false" ];then
	python build.py
else
	python build.py
	isSecure="SIG"
fi

cd ${prodir}

source LA.UM.5.6/LINUX/android/build/core/build_id.mk

if [ $2 = "true" ];then
	#sw_dir=/media/MainDisk/jenki_build/release_sw/SW/$3/$4/${BUILD_ID}$5
	sw_dir=${prodir}/SW_DIR
	outdir=${sw_dir}/QFIL_${isSecure}_$4_${TARGET_VERSION}_${PROJECT_DT2}$5
else
	#sw_dir=/media/MainDisk/jenki_build/release_sw/SW/$3/$4/${BUILD_ID}$5
	sw_dir=${prodir}/SW_DIR
	outdir=${sw_dir}/QFIL_${isSecure}_$4_${TARGET_VERSION}_${PROJECT_DT2}$5
fi

if [ ! -d "$sw_dir"	]; then  
	mkdir -p "$sw_dir"  
fi

rm ${sw_dir}/QFIL_${isSecure}_$4_${TARGET_VERSION}_*.zip
#rm ${sw_dir}/QFIL*.zip

if [ ! -d "$outdir"	]; then  
	mkdir -p "$outdir"  
fi

cp MSM8953.LA.2.0/common/build/bin/asic/NON-HLOS.bin  $outdir/
cp MSM8953.LA.2.0/common/build/bin/asic/sparse_images/*.img $outdir/
cp MSM8953.LA.2.0/common/build/gpt_both0.bin $outdir/
cp MSM8953.LA.2.0/common/build/bin/asic/sparse_images/rawprogram_unsparse.xml  $outdir/
cp MSM8953.LA.2.0/common/build/patch0.xml $outdir/
cp MSM8953.LA.2.0/common/build/gpt_main0.bin $outdir/
cp MSM8953.LA.2.0/common/build/gpt_backup0.bin $outdir/
cp TZ.BF.4.0.5/trustzone_images/build/ms/bin/SANAANAA/lksecapp.mbn $outdir/
cp TZ.BF.4.0.5/trustzone_images/build/ms/bin/SANAANAA/cmnlib.mbn $outdir/
cp TZ.BF.4.0.5/trustzone_images/build/ms/bin/SANAANAA/cmnlib64.mbn $outdir/
cp TZ.BF.4.0.5/trustzone_images/build/ms/bin/SANAANAA/keymaster.mbn $outdir/
cp BOOT.BF.3.3/boot_images/build/ms/bin/JAADANAZ/prog_emmc_firehose_8953_ddr.mbn $outdir/
if [ $1 = "true" ];then
	cp MSM8953.LA.2.0/common/sectools/resources/build/fileversion2/sec.dat $outdir/
fi

cp LA.UM.5.6/LINUX/android/out/target/product/msm8953_64/boot.img $outdir/
cp LA.UM.5.6/LINUX/android/out/target/product/msm8953_64/recovery.img $outdir/
cp LA.UM.5.6/LINUX/android/out/target/product/msm8953_64/mdtp.img $outdir/
cp LA.UM.5.6/LINUX/android/out/target/product/msm8953_64/emmc_appsboot.mbn $outdir/
cp LA.UM.5.6/LINUX/android/device/qcom/common/display/logo/splash.img $outdir/

rm -f ${outdir}*.zip
zip -rj  ${outdir}.zip $outdir/*
#rcrValue=$(./mycksum ${outdir}.zip)
#mv ${outdir}.zip ${outdir}_${rcrValue}.zip
rm -rf $outdir


mkdir -p $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/ROM

mv ${outdir}.zip $OUTPUT_DIR/ROM/
