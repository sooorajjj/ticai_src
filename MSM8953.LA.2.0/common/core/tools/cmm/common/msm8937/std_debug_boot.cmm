//============================================================================
//  Name:                                                                     
//    std_debug_boot.cmm 
//
//  Description:                                                              
//    Boot debug script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 07/13/2015 c_gunnan      Created for 8976
// 05/11/2015 JBILLING      Fixes for command line functionality
// 02/05/2015 JBILLING      remove /noclear so that symbols reset each time
// 02/04/2015 JBILLING      PBL workaround added for UFS
// 02/02/2015 JBILLING      Ported for 8996
// 05/12/2014 AJCheriyan    Moved to the new design
// 10/10/2012 AJCheriyan    Moved to actual reset based debug mechanism
// 08/14/2012 AJCheriyan    Fixed typo.
// 07/19/2012 AJCheriyan    Created for B-family.

//* Arguments passed *//
ENTRY %LINE &ArgumentLine
LOCAL &SubRoutine &Port
LOCAL &targetprocessor &targetprocessorport  &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command  &extraoption &Unused1 &Unused2 &Unused3

//* Check if we need to call for help or do some work *//
IF ("&ArgumentLine"=="")
(
    &SubRoutine="HELP"
)
ELSE
(
    &SubRoutine="MAIN"
)

GOSUB &SubRoutine


MAIN:
    LOCAL &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
    //note that optextract has a limit of 10 entries
    do optextract Img,Lpm,Bkpts,CTI_Enable,alternateelf,extraoption &ArgumentLine
    ENTRY &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption //expect 6 returns from optextract
    // Get the target processor for this port
    do std_debug_&CHIPSET GETDEBUGDEFAULTS NONE &image
    ENTRY &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
    

    do hwio


    // For Apps PBL
    IF ("&image"=="appspbl")
    (
        sys.u
        do std_intercom_do &targetprocessorport std_utils SETBREAKPOINTS Onchip &user_defined_bkpts
        GO
        
        GOTO EXIT

    )

    // We have work to do if it is SBL
    IF ("&image"=="sbl")
    (

        // Load the symbols on the remote session
        //do std_intercom_do &targetprocessorport std_loadsyms_sbl sbl &imagebuildroot
        
        // Attach to Apps Core 0 and break
        do std_intercom_do &targetprocessorport std_utils BREAKPROC

        // Now set the entry and exit breakpoints
        do std_intercom_cmds &targetprocessorport NOWAIT Break.Set &entry_bkpt &error_bkpts /onchip
        
        GO
        
        LOCAL &counter
        &counter=0x0
        WHILE (STATE.RUN())&&(&counter<50.)
        (
            &counter=&counter+1.
            wait.100ms
        
        )

        IF &counter==50.
        (
            WINPOS 0. 0. 50. 10.
            area.reset
            area
            PRINT %ERROR "   Apps Processor did not stop at entry. "
            PRINT " "
            PRINT %ERROR "   Please Check if:"
            PRINT %ERROR "       - Symbols mismatch,"
            PRINT %ERROR "       - SBL image loaded"
            PRINT " "
            PRINT " "
            PRINT " "
            GOTO EXIT
        )
        
        // Set the breakpoints specified by the user
        IF ("&user_defined_bkpts"!="NULL")
        (
            do std_intercom_do &targetprocessorport std_utils SETBREAKPOINTS ONCHIP &user_defined_bkpts
        )
        
        WINPOS 0. 0. 
        DATA.LIST
        WINPOS 0. 50%
        REGISTER /SL
        //If user did not specify entry breakpoint, then go.
        IF (STRING.SCAN("&user_defined_bkpts","&entry_bkpt",0)!=-1)
        (
            GO
        )
        
        
    )


EXIT:
    ENDDO

FATALEXIT:
    END


    
        
